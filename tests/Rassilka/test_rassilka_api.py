import pytest
from rest_framework.test import APIClient
from model_bakery import baker


from Rassilka.models import Mailing, Client, Message

@pytest.fixture
def client():
    return APIClient()

@pytest.fixture
def client_factory():
    def factory(*args, **kwargs):
        return baker.make(Client, *args, **kwargs)
    return factory

@pytest.fixture
def mailing_factory():
    def factory(*args, **kwargs):
        return baker.make(Mailing, *args, **kwargs)
    return factory

@pytest.fixture
def message_factory():
    def factory(*args, **kwargs):
        return baker.make(Message, *args, **kwargs)
    return factory


@pytest.mark.django_db
def test_listMailing(client, mailing_factory):

    mailing = mailing_factory(_quantity=5)

    response = client.get('/api/mailings/')

    assert len(response.json()) == len(mailing)

@pytest.mark.django_db
def test_createMailing(client, mailing_factory):

    mailing = mailing_factory(_quantity=5)

    for i in mailing:

        base = Mailing.objects.count()

        response = client.post('/api/mailings/', data={'date_time_start': i.date_time_start,
                                                       'message_text': i.message_text,
                                                       'date_time_end': i.date_time_end,
                                                       'mobile_codes': i.mobile_codes,
                                                       'tags': i.tags
                                                       })

        assert response.json()['message_text'] == i.message_text
        assert response.status_code == 201
        assert Mailing.objects.count() == base + 1

@pytest.mark.django_db
def test_oneMailing(client, mailing_factory):


    mailing = mailing_factory(_quantity = 5)

    for i in mailing:

        response = client.get('/api/mailings/'+str(i.id)+'/')

        assert response.json()['id'] == int(i.id)

@pytest.mark.django_db
def test_deleteMailing(client, mailing_factory):


    mailing = mailing_factory(_quantity=5)

    base = Mailing.objects.count()

    for i in mailing:

        response_post = client.post('/api/mailings/', data={'date_time_start': i.date_time_start,
                                                       'message_text': i.message_text,
                                                       'date_time_end': i.date_time_end,
                                                       'mobile_codes': i.mobile_codes,
                                                       'tags': i.tags
                                                       })

        assert Mailing.objects.count() == base + 1

        response_delete = client.delete('/api/mailings/'+str(i.id)+'/')

        assert response_delete.status_code == 204
        assert Mailing.objects.count() == base

@pytest.mark.django_db
def test_patchMailing(client, mailing_factory):

    mailing = mailing_factory(_quantity=5)

    for i in mailing:

        response_post = client.post('/api/mailings/', data={'date_time_start': i.date_time_start,
                                                       'message_text': i.message_text,
                                                       'date_time_end': i.date_time_end,
                                                       'mobile_codes': i.mobile_codes,
                                                       'tags': i.tags
                                                       })

        response_patch = client.patch('/api/mailings/'+str(i.id)+'/', data={'message_text': 'C2'})

        assert response_patch.status_code == 200
        assert response_patch.json()['message_text'] == 'C2'

@pytest.mark.django_db
def test_createClient(client, client_factory):

    clients = client_factory(_quantity=5)

    for i in clients:

        base = Client.objects.count()

        response = client.post('/api/clients/', data={   "tel_number": i.tel_number,
                                                          "code": i.code,
                                                          "tag": i.tag,
                                                          "time_zone": i.time_zone
                                                       })

        assert response.json()['tel_number'] == i.tel_number
        assert response.status_code == 201
        assert Client.objects.count() == base + 1


@pytest.mark.django_db
def test_deleteClient(client, client_factory):


    clients = client_factory(_quantity=5)

    base = Client.objects.count()

    for i in clients:

        response_post = client.post('/api/clients/', data={"tel_number": i.tel_number,
                                                          "code": i.code,
                                                          "tag": i.tag,
                                                          "time_zone": i.time_zone
                                                       })

        assert Client.objects.count() == base + 1

        response_delete = client.delete('/api/clients/'+str(i.id)+'/')

        assert response_delete.status_code == 204
        assert Mailing.objects.count() == 0

@pytest.mark.django_db
def test_patchClient(client, client_factory):

    clients = client_factory(_quantity=5)

    for i in clients:

        response_post = client.post('/api/clients/', data={"tel_number": i.tel_number,
                                                          "code": i.code,
                                                          "tag": i.tag,
                                                          "time_zone": i.time_zone
                                                       })

        assert response_post.status_code == 201

        response_patch = client.patch('/api/clients/'+str(i.id)+'/', data={'tel_number': i.tel_number})

        assert response_patch.status_code == 200
        assert response_patch.json()['tel_number'] == i.tel_number

