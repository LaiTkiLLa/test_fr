from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone

# Create your models here.

class Mailing(models.Model):
    date_time_start = models.DateTimeField(verbose_name='Дата и время начала рассылки')
    message_text = models.CharField(max_length=200, verbose_name='Сообщение')
    date_time_end = models.DateTimeField(verbose_name='Дата и время окончания рассылки')
    mobile_codes = models.CharField('Мобильные коды', max_length=50, blank=True)
    tags = models.CharField('Тэги поиска', max_length=50, blank=True)

    @property
    def to_send(self):
        now = timezone.now()
        if self.date_time_start <= now <= self.date_time_end:
            return True
        else:
            return False

    @property
    def sent_messages(self):
        return len(self.messages.filter(status='sent'))

    @property
    def messages_to_send(self):
        return len(self.messages.filter(status='proceeded'))

    @property
    def unsent_messages(self):
        return len(self.messages.filter(status='failed'))

    def __str__(self):
        return f'Рассылка {self.id} от {self.date_time_start}'

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'



class Client(models.Model):
    tel_number = models.PositiveIntegerField(verbose_name='Мобильный телефон')
    code = models.PositiveIntegerField(verbose_name='Код оператора')
    tag = models.CharField(verbose_name='Тэги поиска', max_length=50, blank=True)
    time_zone = models.CharField(verbose_name='Часовой пояс', max_length=10)


    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return f'Клиент {self.id} с номером {self.tel_number}'

    

class Message(models.Model):

    SENT = "отправлено"
    PROCEEDED = "в процессе"
    FAILED = "не отправлено"

    STATUS_CHOICES = [
        (SENT, "отправлено"),
        (PROCEEDED, "в процессе"),
        (FAILED, "не отправлено"),
    ]

    date_time = models.DateTimeField(verbose_name='Дата отправки', auto_now_add=True)
    status = models.CharField(verbose_name='Статус', max_length=15, choices=STATUS_CHOICES)
    message = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='messages')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return f'Сообщение {self.id} с текстом {self.message} для {self.client}'




