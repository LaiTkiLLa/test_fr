from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets
from rest_framework.decorators import action
# from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response

from .serializers import ClientSerializer, MessageSerializer, MailingSerializer
from .models import Client, Mailing, Message


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    """
    Создание клиента
    """

    def post(self, request):
        serializer = ClientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

        return Response({'status': 'OK'})

class ClientPatchDelete(viewsets.ModelViewSet):

    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    """
    Изменение клиента
    """

    def patch(self, request, pk):
        client = Client.objects.get(pk=pk)
        serializer = ClientSerializer(client, data=request.data)
        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data)

    """
    Удаление клиента
    """

    def delete(self, request, pk):
        client = Client.objects.get(pk=pk)
        serializer = ClientSerializer(client, data=request.data)
        if serializer.is_valid():
            serializer.delete()

        return Response('client was deleted successfully!')


class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()


class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    def get(self, request):
        """
        данные по всем рассылкам
        """
        queryset = Mailing.objects.all()
        serializer = MailingSerializer(queryset, many=True)
        return Response(serializer.data)

    """
    создание рассылки
    """

    def post(self, request):
        serializer = MailingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

        return Response({'status': 'OK'})

class MailingGetDelete(viewsets.ModelViewSet):
    """
    данные по конкретной рассылкам
    """
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    def get(self, request, pk):

        queryset = Mailing.objects.all()
        mailing = get_object_or_404(queryset, pk=pk)
        serializer = MailingSerializer(mailing)
        return Response(serializer.data)

    """
    Обновление рассылки
    """

    def patch(self, request, pk):
        mailing = Mailing.objects.get(pk=pk)
        serializer = ClientSerializer(mailing, data=request.data)
        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data)

    """
    Удаление рассылки
    """

    def delete(self, request, pk):

        mailing = get_object_or_404.objects.get(pk=pk)
        serializer = MailingSerializer(mailing, data=request.data)
        if serializer.is_valid():
            serializer.delete()

        return Response('mailing was deleted successfully!')

