from django.apps import AppConfig


class RassilkaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Rassilka'
